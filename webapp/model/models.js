sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device",
    "sap/ui/model/Filter",
    "sap/m/MessageBox"
], 
    /**
     * provide app-view type models (as in the first "V" in MVVC)
     * 
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel
     * @param {typeof sap.ui.Device} Device
     * 
     * @returns {Function} createDeviceModel() for providing runtime info for the device the UI5 app is running on
     */
    function (JSONModel, Device,Filter,MessageBox) {
        "use strict";

        return {
            createDeviceModel: function () {
                var oModel = new JSONModel(Device);
                oModel.setDefaultBindingMode("OneWay");
                return oModel;
        },
        createJSONModel: function () {
            var oModel = new sap.ui.model.json.JSONModel({
                selectedBtn: true,
                ReviewClicked:false,
				MatnrTokensFlag: false,
                countSelectedRows: 0,
                PharmacyZr: 0
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		DirectOrderReason: function(){
            var aFilters = [];
			return new Promise(function(resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").read("/DirectOrderReasonSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
        loadDirectOrder: function(sOrderType,sOrderNum, sSloc){
            var aFilters = [];
			aFilters.push(new Filter("IvOrderNum", sap.ui.model.FilterOperator.EQ, sOrderNum || ''));
            aFilters.push(new Filter("IvOrderType", sap.ui.model.FilterOperator.EQ, sOrderType));
            aFilters.push(new Filter("IvSloc", sap.ui.model.FilterOperator.EQ, sSloc));
            
			return new Promise(function(resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").read("/DirectOrderGetDataSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
		},
		LoadPilotCheck: function (sUser) {
			const sKey = oComponent_PharmOrder.getModel("ODATA").createKey("/PilotCheckSet", {
				IvUserName: sUser
			});
			return new Promise(function(resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").read(sKey, {
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
        LoadUserLgort: function () {
            var aFilters = [];
			aFilters.push(new Filter("IvUname", sap.ui.model.FilterOperator.EQ, ''));
			return new Promise(function(resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").read("/GetStoMrpListSet", {
                    filters: aFilters,
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	
        },
        createOrder: function(oEntry){
            return new Promise(function(resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").create("/DirectOrderHeaderSet", oEntry,{
					success: function(data) {
						resolve(data);
					},
					error: function(error) {
						reject(error);

						console.log(error);

					}
				});
			});	

        },
		PharmacyOpenAfternoon: function (sSloc) {
			const sKey = oComponent_PharmOrder.getModel("ODATA").createKey("/PharmacyOpenAfternoonSet", {
				IvSloc: sSloc
			});
			return new Promise(function (resolve, reject) {
				oComponent_PharmOrder.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
        handleErrors: function (error) {
			try {
				MessageBox.error(JSON.parse(error.responseText).error.message.value);
			} catch (e) {
				MessageBox.error(JSON.stringify(error.message));
			}
		}
    };
});