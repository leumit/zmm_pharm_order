sap.ui.define([], function () {
    "use strict";
    return {
        broto: function(count , price) {
			return (Number(count)*Number(price)).toFixed(2) ;
		},
        getAppTitle: function(sType, sVendor , sVendorName , sOrderNumber) {
            var i18n = this.getView().getModel("i18n").getResourceBundle();
            var text = !!sOrderNumber ? i18n.getText("change") : i18n.getText("create");
            text = text +' ' + i18n.getText(sType);
            if(!!sOrderNumber){
                text+= ' ' + ':' +sOrderNumber;
            }
            if(sType === 'ZMH' && !!sVendor){
                text+= ' ' + i18n.getText("fromVendor") + ' ' + sVendor + ' '+ sVendorName;
            }
            return text;
        }
    };
});
