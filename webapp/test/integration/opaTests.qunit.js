/* global QUnit */

sap.ui.require(["zmm_pharm_order/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
