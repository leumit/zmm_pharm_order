/* global oComponent_PharmOrder  : true */
sap.ui.define([
    "zmm_pharm_order/controller/BaseController",
    "zmm_pharm_order/model/models",
    'sap/m/MessageToast',
    'sap/ui/table/TablePersoController',
    'sap/m/MessageBox'
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, models, MessageToast, TablePersoController, MessageBox) {
        "use strict";

        return Controller.extend("zmm_pharm_order.controller.Main", {
            onInit: function (oEvent) {
                oComponent_PharmOrder._MainController = this;
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.getRoute('Main').attachPatternMatched(this.onMainRoute, this);
                oRouter.getRoute('Default').attachPatternMatched(this.onMainRoute, this);


            },
            onMainRoute: function (oEvent) {
                try {
                    var App = oComponent_PharmOrder.getComponentData().startupParameters["App"][0];
                } catch (e) {
                    var App = "ZR";
                }
                oComponent_PharmOrder.getModel("JSON").setProperty("/App", App);
                try {
                    var sOrderNum = oEvent.getParameter("arguments").OrderNumber;
                } catch (e) {
                    var sOrderNum = '';
                }
                try {
                    var sMrpArea = oEvent.getParameter("arguments").MrpArea;
                } catch (e) {
                    var sMrpArea = '';
                }

                oComponent_PharmOrder.getModel("JSON").setProperty("/OrderNumber", sOrderNum || '');
                oComponent_PharmOrder.getModel("JSON").setProperty("/MrpArea", sMrpArea);
                // 
                // oComponent_PharmOrder.getModel("JSON").setProperty("/OrderNumber", sOrderNum || '');
                var items = oComponent_PharmOrder.getModel("JSON").getProperty("/itemsTable");
                if (!items) {
                    // var sUser = 'SAPIRA'
                    var sUser = new sap.ushell.services.UserInfo().getUser().getId();
                    this.getPilotCheck(App, sOrderNum, sUser, sMrpArea);
                    // this.getUserLgort(App,sOrderNum);
                }
                // this.getData(App,'','C100');

            },
            getDirectOrderReason: function () {
                oComponent_PharmOrder.getModel("ODATA").metadataLoaded().then(() => {
                    models.DirectOrderReason().then(function (data) {
                        var aItems = [];
                        for (var i = 0; i < data.results.length; i++) {
                            aItems[data.results[i].Bsgru] = { text: data.results[i].Bezei };
                        }
                        oComponent_PharmOrder.getModel("JSON").setProperty("/DirectOrderReason", aItems);

                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });

            },
            getPilotCheck: function (App, sOrderNum, sUser, sMrpArea) {
                oComponent_PharmOrder.getModel("ODATA").metadataLoaded().then(() => {
                    models.LoadPilotCheck(sUser).then(function (data) {
                        if (data.EvAuthorized) {
                            oComponent_PharmOrder._MainController.getUserLgort(App, sOrderNum, sMrpArea);
                        }
                        else {
                            MessageBox.error(
                                oComponent_PharmOrder.i18n('pilotMsg'),
                                {
                                    onClose: function (sAction) {
                                        oComponent_PharmOrder._MainController.onNavBack();
                                    }
                                }
                            );
                        }


                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });

            },
            getUserLgort: function (sApp, sOrderNum, sMrpArea) {
                var that = this;
                var bDefault = false;
                oComponent_PharmOrder.getModel("ODATA").metadataLoaded().then(() => {
                    models.LoadUserLgort().then(function (data) {
                        oComponent_PharmOrder.getModel("JSON").setProperty("/userData", data.results);
                        if (!!sMrpArea) {
                            for (var i = 0; i < data.results.length; i++) {
                                if (data.results[i].Berid === sMrpArea) {
                                    bDefault = true;
                                    var sDefaultLgobe = data.results[i];
                                    oComponent_PharmOrder.getModel("JSON").setProperty("/userDataDefault", sDefaultLgobe);

                                }

                            }
                        }
                        else {
                            for (var i = 0; i < data.results.length; i++) {
                                if (data.results[i].Default) {
                                    bDefault = true;
                                    var sDefaultLgobe = data.results[i];
                                    oComponent_PharmOrder.getModel("JSON").setProperty("/userDataDefault", sDefaultLgobe);

                                }

                            }
                        }
                        if (data.results.length === 0 || !bDefault) {
                            oComponent_PharmOrder.getModel("JSON").setProperty("/userDataDefault", { Lgort: '', Lgobe: '' })
                            oComponent_PharmOrder._MainController.onOpenDialog('', 'ChangePharmacy');
                        }
                        else {
                            oComponent_PharmOrder._MainController.getData(sApp, sOrderNum, sDefaultLgobe.Lgort, false);
                        }

                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });
            },
            getData: function (sType, sOrderNum, sSloc, bAfterCheckPharmacyIsOpen) {
                var that = this;
                if (sType === 'ZU' && !bAfterCheckPharmacyIsOpen) {
                    oComponent_PharmOrder._MainController.getPharmacyOpenAfternoon(sType, sOrderNum, sSloc);
                    return;
                }
                oComponent_PharmOrder.getModel("ODATA").metadataLoaded().then(() => {
                    models.loadDirectOrder(sType, sOrderNum, sSloc).then(function (data) {

                        if (data.results.length > 0) {
                            var selectedItemsIndex = 0;
                            for (var i = 0; i < data.results.length; i++) {
                                var aUnit = [];
                                if (!!data.results[i].BaseUnit) {
                                    aUnit.push({ unit: data.results[i].BaseUnit });
                                }
                                if (!!data.results[i].OrderUnit) {
                                    aUnit.push({ unit: data.results[i].OrderUnit });
                                }
                                if (!!sOrderNum && data.results[i].Selected) {
                                    selectedItemsIndex++;
                                }

                                that.removeDuplicates(aUnit, 'unit');
                                data.results[i].OrderQun = parseInt(data.results[i].OrderQun).toString();
                                data.results[i].total = (Number(data.results[i].OrderQun) * Number(data.results[i].UnitPrice)).toFixed(2);
                                data.results[i].SlocInventoryInt = parseInt(data.results[i].SlocInventory);
                                data.results[i].MatnrInt = parseInt(data.results[i].Matnr);
                                data.results[i].UnitArray = aUnit;
                            }

                        }
                        else {
                            if (sType === 'ZMH') {
                                MessageToast.show(oComponent_PharmOrder.i18n('noDataFoundZMH'), {
                                    duration: 2000
                                });
                            }
                            else {
                                MessageToast.show(oComponent_PharmOrder.i18n('noDataFound'), {
                                    duration: 2000
                                });
                            }

                            // setTimeout(function () { oComponent_PharmOrder._MainController.onNavBack(); return; }, 1500);
                        }
                        oComponent_PharmOrder.getModel("JSON").setProperty("/itemsTable", data.results);
                        oComponent_PharmOrder.getModel("JSON").setProperty("/countSelectedRows", selectedItemsIndex);
                        oComponent_PharmOrder._MainController.getDirectOrderReason();

                    }).catch((error) => {
                        models.handleErrors(error);
                    });
                });
            },
            getPharmacyOpenAfternoon: function (sType, sOrderNum, sSloc) {
                var that = this;
                models.PharmacyOpenAfternoon(sSloc).then(function (data) {
                    if (data.EvIsOpen) {
                        oComponent_PharmOrder._MainController.getData(sType, sOrderNum, sSloc, true);
                    }
                    else {
                        that.onOpenDialog('', 'PharmacyCloseAfternoon');
                    }
                }).catch((error) => {
                    models.handleErrors(error);
                });
            },
            beforeOpenChangePharmacy: function (oEvent) {
                MessageBox.warning(oComponent_PharmOrder.i18n('ChangePharmacyMsg'), {
                    actions: ['ביטול', 'אישור'],
                    emphasizedAction: 'אישור',
                    onClose: function (sAction) {
                        if (sAction === 'אישור') {
                            oComponent_PharmOrder._MainController.onOpenDialog(oEvent, 'ChangePharmacy');
                        }

                    }
                });
            },

            onChangePharmacy: function (oEvent) {
                this.onCloseDialog('PharmacyCloseAfternoon');
                this.onOpenDialog(oEvent, 'ChangePharmacy');

            },
            beforeReview: function (sType) {
                var aItems = [];
                var rows = oComponent_PharmOrder.getModel("JSON").getProperty("/itemsTable");
                var sAppType = oComponent_PharmOrder.getModel("JSON").getProperty("/App");
                if (sType === '2' && !!oComponent_PharmOrder.getModel("JSON").getProperty("/OrderNumber")) {
                    sType === '4';
                }

                if (sType !== '2') {
                    oComponent_PharmOrder.getModel("JSON").setProperty("/ReviewClicked", true);
                }
                for (var i = 0; i < rows.length; i++) {
                    if (!!rows[i].Selected) {
                        if (Number(rows[i].OrderQun) === 0 && sType !== '2') {
                            MessageToast.show(oComponent_PharmOrder.i18n('noCountMsg'), {
                                duration: 2000
                            });
                            return;
                        }
                        else if ((sAppType === 'ZR' || sAppType === 'ZTBP') && Number(rows[i].OrderQun) > Number(rows[i].SlocInventory) && sType !== '2') {
                            MessageToast.show(oComponent_PharmOrder.i18n('SlocInventoryError'), {
                                duration: 2000
                            });
                            return;
                        }
                        else if (sAppType === 'ZR' && !rows[i].ReturnReason && sType !== '2') {
                            MessageToast.show(oComponent_PharmOrder.i18n('enterReturnReason'), {
                                duration: 2000
                            });
                            return;
                        }
                        else if (sAppType === 'ZR' && !rows[i].ReturnReasonDesc) {
                            var aReasons = oComponent_PharmOrder.getModel("JSON").getProperty('/DirectOrderReason');
                            if (!!aReasons[rows[i].ReturnReason]) {
                                rows[i].ReturnReasonDesc = aReasons[rows[i].ReturnReason].text;
                            }
                            aItems.push(rows[i]);
                        }
                        else {
                            aItems.push(rows[i]);
                        }

                    }
                }
                if (sAppType === 'ZR') {
                    var reason3Index = 0;
                    for (var i = 0; i < aItems.length; i++) {
                        if (aItems[i].ReturnReason === '3') {
                            reason3Index++;
                        }
                    }
                    if (reason3Index > 0 && reason3Index !== aItems.length) {
                        var aErrors = [];
                        aErrors.push({
                            Type: "E",
                            Message: oComponent_PharmOrder.i18n('reason3ErrorMsg')
                        });
                        this.showErrorsMsg(aErrors);
                        // MessageToast.show(oComponent_PharmOrder.i18n('reason3ErrorMsg'), {
                        //     duration: 2000
                        // });
                        return;
                    }
                }
                if (aItems.length < 1) {
                    MessageToast.show(oComponent_PharmOrder.i18n('noSelectedItemsMsg'), {
                        duration: 1500
                    });
                }
                else {
                    if (sType === '2' && sAppType === 'ZTBP' && !rows[0].StorageLocation) {
                        oComponent_PharmOrder.getModel("JSON").setProperty("/selectedItemsZTBP", aItems);
                        this.onOpenDialog('', 'ChangePharmacyZtbpDraft');
                    }
                    else {
                        oComponent_PharmOrder.getModel("JSON").setProperty("/selectedItems", aItems);
                        this.onCreateOrder(sType, aItems);
                    }
                }

            },
            onDeleteOrder: function () {
                var aItems = [];
                this.onCreateOrder('5', aItems);
            },
            onSelectReturnReason: function (oEvent) {
                var sDesc = oEvent.getParameter("selectedItem").getProperty("text");
                var sPath = oEvent.getSource().getBindingContext("JSON").getPath();
                oComponent_PharmOrder.getModel("JSON").setProperty(sPath + '/ReturnReasonDesc', sDesc);
            },
            // beforeReview: function () {
            //     var aItems = [];
            //     var rows = oComponent_PharmOrder.getModel("JSON").getProperty("/itemsTable");
            //     oComponent_PharmOrder.getModel("JSON").setProperty("/ReviewClicked", true);
            //     for (var i = 0; i < rows.length; i++) {
            //         if (!!rows[i].Selected) {
            //             if (Number(rows[i].OrderQun) === 0) {
            //                 MessageToast.show(oComponent_PharmOrder.i18n('noCountMsg'), {
            //                     duration: 1500
            //                 });
            //                 return;
            //             }
            //             else {
            //                 aItems.push(rows[i]);
            //             }

            //         }
            //     }
            //     if (aItems.length < 1) {
            //         MessageToast.show(oComponent_PharmOrder.i18n('noSelectedItemsMsg'), {
            //             duration: 1500
            //         });
            //     }
            //     else {
            //         oComponent_PharmOrder.getModel("JSON").setProperty("/selectedItems", aItems);
            //         this.navToReview();
            //     }

            // },
            onTableRowSelected: function (oEvent, items) {
                try {
                    oEvent.getSource().getBindingContext("JSON").getObject().Selected = oEvent.getParameter("selected");
                } catch { }
                var allItems = items || oComponent_PharmOrder.getModel("JSON").getProperty("/itemsTable"),
                    countSelectedRows = 0;
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        countSelectedRows++;
                    }
                }
                oComponent_PharmOrder.getModel("JSON").setProperty("/countSelectedRows", countSelectedRows);

            },
            navToReview: function (oEvent) {
                sap.ui.core.UIComponent.getRouterFor(this).navTo("Review");
            }
        });
    });
