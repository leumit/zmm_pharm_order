/* global oComponent_PharmOrder  : true */
sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "zmm_pharm_order/model/formatter",
        "zmm_pharm_order/model/models",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
        "sap/ui/export/Spreadsheet",
        "sap/m/MessageBox",
        'sap/m/Token'
    ],
    function (Controller, History, UIComponent, formatter, models, Filter, FilterOperator, Spreadsheet, MessageBox, Token) {
        "use strict";

        return Controller.extend("zmm_pharm_order.controller.BaseController", {
            formatter: formatter,
            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },
            onNavBack: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({
                    target: { shellHash: "#" }
                });

            },
            onOpenDialog: function (oEvent, sDialogName) {
                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("zmm_pharm_order.view.fragments." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onCloseDialog: function (sDialogName) {
                this[sDialogName].close();
            },
            afterMessageViewClose: function (oEvent) {
                oComponent_PharmOrder._MessageView.destroy();
                oComponent_PharmOrder._MessageView = undefined;

            },
            //  beforeOpenMessageView: function(oEvent){
            //     debugger;
            //     try{
            //         sap.ui.getCore().byId("messageView").navigateBack();
            //     }catch(e){}
            //  },
            enterOrderCount: function (oEvent) {
                var _oInput = oEvent.getSource();
                var val = oEvent.getParameter("value");
                var path = oEvent.getSource().getBindingContext("JSON").getPath();
                var object = oEvent.getSource().getBindingContext("JSON").getObject()
                val = val.replace(/[^0-9/.]/g, '');
                if (val.indexOf(".") !== -1 && val.split(".")[1].length > 3) {
                    val = parseFloat(val).toFixed(3);
                }
                _oInput.setValue(val);
                oComponent_PharmOrder.getModel("JSON").setProperty(path + "/total", (Number(val) * Number(object.UnitPrice)).toFixed(2));

            },
            onPressComment: function (oEvent, sType) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                oComponent_PharmOrder.getModel("JSON").setProperty("/oSelectedRow", oSelectedItem);
                oComponent_PharmOrder.getModel("JSON").setProperty("/page", sType);
                oComponent_PharmOrder.getModel("JSON").setProperty("/newItemText", oSelectedItem.ItemText);
                this.onOpenDialog('', 'comment');
            },
            saveCommentRow: function (oEvent) {
                var oModel = oComponent_PharmOrder.getModel("JSON"),
                    oRow = oModel.getProperty("/oSelectedRow");
                oRow.ItemText = oModel.getProperty("/newItemText");
                oComponent_PharmOrder.getModel("JSON").refresh();
                this.onCloseDialog('comment');
            },
            onValueHelpSearch: function (oEvent, sType) {
                var sValue = oEvent.getParameter("value");
                var oFilter = [];
                if (sType === 'Lgobe' || sType === 'Pharmacy') {
                    oFilter = new Filter({
                        filters: [new Filter("Lgobe", FilterOperator.Contains, sValue),
                        new Filter("Lgort", FilterOperator.Contains, sValue),
                        new Filter("Lgort", FilterOperator.Contains, sValue.toUpperCase())
                        ],
                        and: false
                    });

                }
                else if (sType === 'Order') {
                    oFilter.push(new Filter("Order", FilterOperator.Contains, sValue));
                }
                else {
                    oFilter = new Filter({
                        filters: [new Filter("Adname", FilterOperator.Contains, sValue),
                        new Filter("Adrnr", FilterOperator.Contains, sValue)
                        ],
                        and: false
                    });
                }
                var oBinding = oEvent.getParameter("itemsBinding");
                oBinding.filter(oFilter);
            },
            onValueHelpClose: function (oEvent, sType) {
                var oSelectedItem = oEvent.getParameter("selectedItem");
                var userLgort = oComponent_PharmOrder.getModel("JSON").getProperty("/userDataDefault/Lgort");

                if (!oSelectedItem) {
                    return;
                }
                if (sType === 'Lgobe') {
                    var oRow = oEvent.getParameter("selectedItem").getBindingContext("ODATA").getObject()
                    var sLgort = oRow.Lgort;
                    if (sLgort !== userLgort) {
                        var sApp = oComponent_PharmOrder.getModel("JSON").getProperty("/App");
                        this.getData(sApp, '', sLgort, false);
                    }
                    oComponent_PharmOrder.getModel("JSON").setProperty("/userDataDefault", oRow);
                }
                if (sType === 'Pharmacy') {
                    var sPharmacy = oSelectedItem.getProperty("description");
                    if (!!sPharmacy) {
                        this.onCreateOrder('3', '', sPharmacy);
                    }
                }
                if (sType === 'PharmacyDraft') {
                    var sPharmacy = oSelectedItem.getProperty("description");
                    if (!!sPharmacy) {
                        var aItems = oComponent_PharmOrder.getModel("JSON").getProperty("/selectedItemsZTBP");
                        for (var i = 0; i < aItems.length; i++) {
                            aItems[i].StorageLocation = sPharmacy;
                        }
                        this.onCreateOrder('2', aItems);
                    }
                }
                oEvent.getSource().getBinding("items").filter([]);

            },
            createColumnConfig: function (oEvent, sTableId) {
                var oController = sTableId === 'mainTable' ? oComponent_PharmOrder._MainController : oComponent_PharmOrder._ReviewController;
                var aCols = oController.byId(sTableId).getColumns();
                var aColumns = [];
                var i = sTableId === 'mainTable' ? 1 : 0;
                for (i; i < aCols.length - 1; i++) {
                    if (aCols[i].getVisible()) {
                        if (aCols[i].getFilterProperty() === 'SlocInventory') {
                            aColumns.push({
                                label: oComponent_PharmOrder.i18n('SlocInventory'),
                                property: ['SlocInventory', 'OpenQuntity'],
                                template: '{0}, ({1})'
                            });
                        }
                        else if(!!aCols[i].getLabel()){
                            aColumns.push({
                                label: aCols[i].getLabel().getProperty("text") || " ",
                                property: aCols[i].getFilterProperty() || " ",
                                width: '15rem'
                            });
                        }


                    }
                }
                return aColumns;

            },
            onExport: function (oEvent, sTableId) {
                var aCols, aItems, oSettings, oSheet;
                var oController = sTableId === 'mainTable' ? oComponent_PharmOrder._MainController : oComponent_PharmOrder._ReviewController;
                aCols = this.createColumnConfig(oEvent, sTableId);
                aItems = oComponent_PharmOrder.getModel("JSON");
                var sApp = oComponent_PharmOrder.getModel("JSON").getProperty("/App");
                var aIndices = oController.byId(sTableId).getBinding("rows").aIndices;
                var path = sTableId === 'mainTable' ? "/itemsTable/" : "/orderItems/";
                var aSelectedModel = [];
                for (var i = 0; i < aIndices.length; i++) {
                    aSelectedModel.push(aItems.getProperty(path + aIndices[i]));
                }
                // var aSelectedModel = oComponent_PharmOrder._MainController.byId("dataTable").getBinding("rows").oList;
                oSettings = {
                    workbook: {
                        columns: aCols
                    },
                    dataSource: aSelectedModel,
                    fileName: oComponent_PharmOrder.i18n(sApp)
                };

                oSheet = new Spreadsheet(oSettings);
                oSheet.build()
                    .then(function () { });

            },
            onCreateOrder: function (type, items, sloc) {
                var oModel = oComponent_PharmOrder.getModel("JSON").getData(),
                    items = items || oModel.selectedItems,
                    aItems = [];
                for (var i = 0; i < items.length; i++) {
                    aItems.push({
                        Item: items[i].Item,
                        MatType: items[i].MatType,
                        Matnr: items[i].Matnr,
                        MatnrTxt: items[i].MatnrTxt,
                        AtcCode: items[i].AtcCode,
                        SupplyType: items[i].SupplyType,
                        OrderQun: (Number(items[i].OrderQun) || '0').toString(),
                        OrderUnit: items[i].OrderUnit,
                        BaseUnit: items[i].BaseUnit,
                        StorageLocation: sloc || items[i].StorageLocation || '',
                        IssueStorageLocation: items[i].IssueStorageLocation,
                        SlocInventory: items[i].SlocInventory,
                        OpenQuntity: items[i].OpenQuntity,
                        ReturnReason: items[i].ReturnReason,
                        UnitPrice: items[i].UnitPrice,
                        Currency: items[i].Currency,
                        Vendor: items[i].Vendor,
                        VendorName: items[i].VendorName,
                        Agrmnt: items[i].Agrmnt,
                        AgrmntItem: items[i].AgrmntItem,
                        CostCenter: items[i].CostCenter,
                        PurchaseOrganization: items[i].PurchaseOrganization,
                        PurchaseGroup: items[i].PurchaseGroup,
                        ItemText: items[i].ItemText,
                        Selected: items[i].Selected

                    })
                }
                var oEntry = {
                    IvActivity: type,
                    IvOrderNum: oModel.OrderNumber || '',
                    IvOrderText: !!oModel.orderText ? oModel.orderText : '',
                    IvOrderType: oModel.App,
                    IvSloc: oModel.userDataDefault.Lgort,
                    EvOrderNum: '',
                    DirectOrderItems: aItems,
                    MessageReturn: []

                }

                models.createOrder(oEntry).then(function (data) {
                    oComponent_PharmOrder.getModel("JSON").setProperty("/OrderNumber", data.EvOrderNum);
                    if (data.MessageReturn.results.length > 0) {
                        this.showErrorsMsg(data.MessageReturn.results);
                    }
                    else if (type === '1') {
                        oComponent_PharmOrder.getModel("JSON").setProperty("/orderItems", oModel.selectedItems);
                        oComponent_PharmOrder._MainController.navToReview();
                    }

                    else if (type === '2') {
                        this.onOpenDialog("", 'saveDefect');
                    }
                    else if (type === '3') {
                        this.onOpenDialog("", 'createOrder');
                    }
                    else if (type === '4') {
                        this.onOpenDialog("", 'saveDefect');
                    }
                    else if (type === '5') {
                        this.onOpenDialog("", 'deleteOrder');
                    }

                }.bind(this)).catch(function (error) {
                    console.log(error);
                    try {
                        MessageBox.error(JSON.parse(error.responseText).error.message.value);
                    } catch (e) {
                        MessageBox.error(JSON.stringify(error));
                    }
                });
            },
            showErrorsMsg: function (aErrors) {
                oComponent_PharmOrder.getModel("JSON").setProperty("/Errors", aErrors);
                this.onOpenMessageViewDialog();

            },
            onOpenMessageViewDialog: function (oEvent) {
                if (!oComponent_PharmOrder._MessageView) {
                    oComponent_PharmOrder._MessageView = sap.ui.xmlfragment("zmm_pharm_order.view.fragments." + 'MessageViewDialog', this);
                    this.getView().addDependent(oComponent_PharmOrder._MessageView);
                }
                oComponent_PharmOrder._MessageView.open();
            },
            onCloseMessageViewDialog: function (oEvent) {
                oComponent_PharmOrder._MessageView.close();
            },
            removeDuplicates: function (array, key) {
                let lookup = new Set();
                return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
            },
            filterItems: function (oEvent, sType, aMatnrNumbers) {
                var oFilter = [];
                if (sType === 'freeText') {
                    var sQuery = oEvent.getSource().getValue();
                }
                else {
                    var sQuery = oComponent_PharmOrder.getModel("JSON").getProperty("/MainSearchDesc");
                }
                var sApp = oComponent_PharmOrder.getModel("JSON").getProperty("/App");
                if (sQuery && sQuery.length > 0) {
                    var filters = [new Filter("Matnr", FilterOperator.Contains, sQuery),
                    new Filter("MatnrTxt", FilterOperator.Contains, sQuery)];
                    if (sApp !== 'ZB') {
                        filters.push(new Filter("AtcCode", FilterOperator.Contains, sQuery));
                    }
                    oFilter.push(new Filter({
                        filters,
                        and: false
                    }));
                }
                if (!!aMatnrNumbers && aMatnrNumbers.length) {
                    var matNumFilter = new Filter({
                        filters: [],
                        and: false
                    });
                    for (var i = 0; i < aMatnrNumbers.length; i++) {
                        matNumFilter.aFilters.push(new Filter('Matnr', "EQ", aMatnrNumbers[i]));
                    }
                    oFilter.push(matNumFilter);


                }

                // update table binding
                var oTable = this.byId("mainTable");
                var oBinding = oTable.getBinding("rows");
                oBinding.filter(oFilter, "Application");
            },
            clearAllFilters: function(oEvent) {
                var oTable = oComponent_PharmOrder._MainController.byId("mainTable");       
                var aColumns = oTable.getColumns();
                for (var i = 0; i < aColumns.length; i++) {
                        aColumns[i].filter(null);
                        aColumns[i].sort(null);
                        aColumns[i].setSorted(false);
                }
                oComponent_PharmOrder.getModel("JSON").setProperty("/MainSearchDesc", '');
                this.filterItems();
                this.onUpdateToken('', true);
            },
            onMultiTokenLiveChange: function (oEvent) {
                const sValues = oEvent.getParameter("newValue");
                const aValues = sValues.split(" ");
                const oMultiInput = oEvent.getSource();

                if (aValues.length > 1) {
                    for (let i in aValues) {
                        if (!!aValues[i]) {
                            let oToken = new sap.m.Token({
                                key: aValues[i],
                                text: aValues[i],
                            });
                            oMultiInput.addToken(oToken);
                        }
                    }
                    setTimeout(() => {
                        oMultiInput.setValue("");
                    }, 0);
                    oMultiInput.fireChange(); //because in copy paste its not work
                }
            },
            onMultiTokenChange: function (oEvent) {
                const sValue = oEvent.getParameter("newValue");
                const oMultiInput = oEvent.getSource();
                if (!!sValue) {
                    let oToken = new sap.m.Token({
                        key: sValue,
                        text: sValue,
                    });
                    oMultiInput.addToken(oToken);
                    setTimeout(() => {
                        oMultiInput.setValue("");
                    }, 0);
                }
                var tokens = oEvent.getSource().getTokens(),
                    aMatnrNumbers = [];
                if (tokens.length) {
                    for (var i = 0; i < tokens.length; i++) {
                        aMatnrNumbers.push(tokens[i].getProperty("key"));
                    }
                }
                this.filterItems(oEvent, 'MatnrList', aMatnrNumbers);
                oComponent_PharmOrder.getModel("JSON").setProperty("/MatnrTokensFlag", tokens.length > 0);


            },
            onUpdateToken: function (oEvent, deleteBtn) {
                //when delete btn pressed
                var oMultiInput = oComponent_PharmOrder._MainController.getView().byId("MatnrMulti");
                if (deleteBtn) {

                    var aRemoveTokens = oMultiInput.getTokens();

                    for (let i in aRemoveTokens) {
                        oMultiInput.removeToken(aRemoveTokens[i]);
                    }
                }
                //when delete number or numbers
                else {
                    const oMultiInput = oEvent.getSource();
                    var aRemoveTokens = oEvent.getParameter("removedTokens");

                    for (let i in aRemoveTokens) {
                        oEvent.getSource().removeToken(aRemoveTokens[i]);
                    }
                }
                oMultiInput.fireChange();
            }


        });
    });
