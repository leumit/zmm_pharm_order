/* global oComponent_PharmOrder  : true */
sap.ui.define(["zmm_pharm_order/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("zmm_pharm_order.controller.Review", {
        onInit: function () {
            oComponent_PharmOrder._ReviewController = this;
            var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            oRouter.getRoute('Review').attachPatternMatched(this.onReview, this);
        },
        onReview: function () {
            // oComponent_PharmOrder.getModel("JSON").setProperty("/page" , 'Review');
            // var items =  oComponent_PharmOrder.getModel("JSON").getProperty("/itemsTable") || '';
            // if(!items){
            //     setTimeout(function () { oComponent_PharmOrder._ReviewController.navTo('', 'Main'); return; }, 1000);
            // }
        },
        navToMain: function (oEvent) {
            sap.ui.core.UIComponent.getRouterFor(this).navTo("Main");
        },
        beforeCreateOrder: function (oEvent, sType, aItems) {
            if (!window.event.detail || window.event.detail == 1) {
                var sApp = oComponent_PharmOrder.getModel("JSON").getProperty("/App");
                if (sApp === 'ZR') {
                    this.onOpenDialog(oEvent, 'ChangePharmacyZr');
                }
                else if (sApp === 'ZTBP' && !aItems[0].StorageLocation) {
                    this.onOpenDialog(oEvent, 'ChangePharmacyZtbp');
                }
                else {
                    this.onCreateOrder(sType, aItems);
                }
            }
        },
        onSelectPharmacyZr: function (oEvent) {
            if (!window.event.detail || window.event.detail == 1) {
                var sPharmacy = oComponent_PharmOrder.getModel("JSON").getProperty("/PharmacyZr");
                sPharmacy = sPharmacy === 1 ? 'NL20' : 'NL10';
                this.onCreateOrder('3', '', sPharmacy);
            }
        }
    });
});
