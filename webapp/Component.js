/* global oComponent_PharmOrder  : true */
var oComponent_PharmOrder;
sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "zmm_pharm_order/model/models"
],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("zmm_pharm_order.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                oComponent_PharmOrder = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                // enable routing
                this.getRouter().initialize();
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");

                // set the device model
                this.setModel(models.createDeviceModel(), "device");
                // set the json model
                this.setModel(models.createJSONModel(), "JSON");
                //busyIndicator
                this.getModel("ODATA").attachBatchRequestSent(function () {
                    sap.ui.core.BusyIndicator.show();
                }).attachBatchRequestCompleted(function (event) {
                    sap.ui.core.BusyIndicator.hide();
                });

                window.addEventListener('beforeunload', oComponent_PharmOrder.beforeUnload, true);

            },
            beforeUnload: function (e) {
                    e.preventDefault();
                    e.returnValue = '';
            },
            destroy: function () {
                window.removeEventListener('beforeunload', oComponent_PharmOrder.beforeUnload, true);
                UIComponent.prototype.destroy.apply(this, arguments);
            },
            i18n: function (str) {
                return oComponent_PharmOrder.getModel("i18n").getProperty(str);
            }
        });
    }
);